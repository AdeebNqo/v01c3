Protocol
========

The first thing we need to consider is the communication
or transmission of important info.The syntax will be of the form:

	[type]:[value]	e.g username:zola

The first thing to transmit after establishing a connection is the username.